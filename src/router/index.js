import HomePage from '@/components/HomePage'
import AboutUsPage from '@/components/AboutUsPage'
import PostPage from '@/components/PostPage'
import UsersPage from '@/components/UsersPage'
import UsersAdminPage from '@/components/users/UsersAdminPage'
import UsersHome from '@/components/users/UsersHome'
import UsersViewPage from '@/components/users/UsersViewPage'






const routes =  [
        {
            path: '/',
            name: "/",
            component: HomePage,
        },
        {
            path: '/about',
            name: "about",
            component: AboutUsPage,
        },
        {
            path: '/posts/:id',
            name: "post",
            component: PostPage,
            props: true
        },
        {
            path: '/users',
            name: "users",
            component: UsersPage,
            children: [
                {
                    path: '',
                    name: "",
                    component: UsersHome,
                },
                {
                    path: 'admin',
                    name: "admin",
                    component: UsersAdminPage,
                },
                {
                    path: ':id',
                    name: "userId",
                    component: UsersViewPage,
                    props: true
                },


        
            ]
        }


    ]

export default routes;